<?
/*
 * m31l1nj3Kt (ex Script Bombmail)
 * Desarrollado por ed00m:th3d00m ed00m@gnulinuxporqueno.cl
 * 
 * Algoritmo de inyeccion masiva de correos para auditoria sobre comportamiento
 * de sistemas ANTI-SPAM como "spamassasin"
 * 
 * Liberado bajo GPL
 * No se ofrece garantia ni responsabilidad sobre el uso malintencionado de
 * este software.
 *
 * 17/08/2009 Script Bombmail v0.1->v0.2
 * 25/08/2009 Script Bombmail v0.2.2->0.2.4
 * 27/08/2009 Script Bombmail v0.2.2.4->v0.2.2.6
 * 17/09/2012 m31l1nj3Kt      V0.1 //Migracion a POO
 */
    
class m31l1nj3Kt {
	/*
	 * Metodos:
	 *      GeneraData
	 *      GeneraCorreos
	 *      Injectar
	 */
	 
	public function Injectar ($cantidad,$destinatarios) {

    	$data = $this->GeneraData();
    	list ($dataAsunto,$dataCuerpo) = explode ("|",$data);
    	
    	for($i=0;$i<$cantidad;$i++){
    		
    		$aRem = rand(5,15);
    		echo "".$aRem."\n";
    		$aProv = rand(2,8);
    		echo "".$aProv."\n";
    		$aTlds = rand(1,4);
    		echo "".$aTlds."\n";    
	 	    $sendMail = $this->GeneraCorreos($aRem,$aProv,$aTlds);
	 	    echo "".$sendMail."\n";
	 	    
	 	    mail($destinatarios,$dataAsunto,$dataCuerpo,"FROM: $sendMail");
	 	    
	 	    
    	}
    	echo "\n* Injector ha terminado\n";
	}
    
    protected function generaCadenas($largo,$listacaracteres) {
        $string = "";
        $i = 0;
        while ($i < $largo) {
            $char = substr($listacaracteres, mt_rand(0, strlen($listacaracteres)-1), 1);
            $string .= $char;
            $i++;
        }
        return $string;
    } 

	 protected function GeneraCorreos($aRem,$aProv,$aTlds) {
    	
    	//Remitentes
        $cadenaRem = "._0123456789abcdfghjkmnpqrstvwxyz";
        $remitente = $this->generaCadenas($aRem,$cadenaRem);
    	
    	//Proveedores
        $cadenaProv = "abcdfghjkmnpqrstvwxyz";
    	$proveedor  = $this->generaCadenas($aProv,$cadenaProv);
    	
    	//tlds
    	$tlds = $this->generaCadenas($aTlds,$cadenaProv);
        
		
		return $remitente."@".$proveedor.".".$tlds;
    	
	 	
	 }
	 protected function GeneraData () {
	 	
	 	$data = Array (
	 					asunto => "Auditoria Anti Spam ".date("H:i"),
	 					cuerpo => "
			..............................................
            Auditoria comportamiento Anti-Spam.

            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Fusce suscipit posuere erat, eget pellentesque risus bibendum in.
            Nullam ac odio orci, a commodo ipsum. 
            Morbi adipiscing tempor tortor ac dapibus.
            Aenean fermentum massa et lacus lacinia accumsan.
            Ut pellentesque justo eu nisl suscipit egestas.
            Quisque porttitor sodales enim, nec elementum urna varius quis.
            Morbi ut sapien lectus.
            Proin elementum luctus eros, non mollis lorem commodo eget.
            Sed imperdiet dignissim erat, id facilisis mi placerat id.
            Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus.
            Aliquam posuere dignissim erat, id laoreet nulla scelerisque et.
            Cras pretium aliquam neque a tempor.
            Fusce sollicitudin eleifend accumsan.
            Vestibulum semper ultrices purus sed rutrum.
            Fusce ultricies bibendum semper

            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Fusce suscipit posuere erat, eget pellentesque risus bibendum in.
            Nullam ac odio orci, a commodo ipsum.
            Morbi adipiscing tempor tortor ac dapibus.
            Aenean fermentum massa et lacus lacinia accumsan.
            Ut pellentesque justo eu nisl suscipit egestas.
            Quisque porttitor sodales enim, nec elementum urna varius quis.
            Morbi ut sapien lectus.
            Proin elementum luctus eros, non mollis lorem commodo eget.
            Sed imperdiet dignissim erat, id facilisis mi placerat id.
            Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus.
            Aliquam posuere dignissim erat, id laoreet nulla scelerisque et.
            Cras pretium aliquam neque a tempor.
            Fusce sollicitudin eleifend accumsan.
            Vestibulum semper ultrices purus sed rutrum.
            Fusce ultricies bibendum semper
            .............................................."
	 	);
	 	return $data[asunto]."|".$data[cuerpo]; 	
	 }

	 
}

try {
	
	$Inyector = new m31l1nj3Kt();
	$commitInjector = $Inyector->Injectar(5,$destinatarios="correo1,correo2,correo3");
	
}catch (Exception $e){
	echo "Error: ".$e->getMessage()."\n";
}


?>
